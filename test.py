import argparse
import getpass
import os

import requests
import urlparse

parser = argparse.ArgumentParser(description='Download all PUP files for a given assessor ID.')
parser.add_argument('-f', '--folder', default='/tmp', help='Where to store the retrieved file')
parser.add_argument('-r', '--resource',
                    default='/data/projects/OASIS3/subjects/OAS30001/experiments/OAS30001_MR_d0757',
                    help='The URI path to a resource, e.g. /data/projects/X/subjects/Y/experiments/Z')
parser.add_argument('-g', '--get',
                    default='/data/experiments/CENTRAL_E09254/resources/123236303/files/dataset_description.json',
                    help='The URI path to a file to get (default requires OASIS3 access)')
parser.add_argument('-s', '--site', default='https://central.xnat.org',
                    help="Site to download from, example https://cnda.wustl.edu (full site url)")
parser.add_argument('-u', '--user', required=False, help="Username or alias (from site/data/services/tokens/issue)")
parser.add_argument('-p', '--password', required=False,
                    help="Password or secret (from site/data/services/tokens/issue)")
args = parser.parse_args()

site = args.site
user = args.user
if user is None:
    user = raw_input("Enter your username for " + site + ": ")
password = args.password
if password is None:
    password = getpass.getpass("Enter your password for " + site + ": ")

auth_url = site + '/data/JSESSION'
folder = args.folder
headers = {"Content-Type": "application/json"}
parameters = {"format": "json"}
credentials = (user, password)
session = requests.Session()


def download_file(folder_path, filename, download, block_sz):
    # from https://stackoverflow.com/a/22776
    # download file in chunks in case it is too large
    f = open(os.path.join(folder_path, filename), 'wb')
    file_size = int(download.headers["Content-Length"])
    print("Downloading: %s Bytes: %s" % (filename, file_size))

    file_size_dl = 0

    for incoming in download.iter_content(chunk_size=block_sz):
        file_size_dl += len(incoming)
        f.write(incoming)
        status = r"%10d  [%3.2f%%]" % (file_size_dl, file_size_dl * 100. / file_size)
        status = status + chr(8) * (len(status) + 1)
        print(status)

    f.close()


try:
    response = session.get(auth_url, headers=headers, params=parameters, auth=credentials)
except requests.exceptions.HTTPError as error:
    print("Well, that didn't work. Got error code " + str(error))
else:
    print("That went well! I got the response: [" + str(response.status_code) + "] " + response.text)

try:
    response = session.get(site + args.resource, headers=headers, params=parameters)
except requests.exceptions.HTTPError as error:
    print("Well, that didn't work. Got error code " + str(error))
else:
    print("That went well! I got the response: [" + str(response.status_code) + "] " + response.text)

try:
    get_url = site + args.get
    response = session.get(get_url, headers=headers, params=parameters)
    file_name = os.path.basename(urlparse.urlparse(get_url).path)
    download_file(args.folder, file_name, response, 8192)
except requests.exceptions.HTTPError as error:
    print("Well, that didn't work. Got error code " + str(error))
else:
    print("That went well! I got the response: [" + str(response.status_code) + "] " + response.text)

try:
    response = session.delete(auth_url)
except requests.exceptions.HTTPError as error:
    print("Well, that didn't work. Got error code " + str(error))
else:
    print("That went well! I got the response: [" + str(response.status_code) + "] " + response.text)
